package mhat.reports;

import mhat.main.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ReportsActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports);

	}

	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.mapButton:
			Intent intent = new Intent(ReportsActivity.this,
					mhat.reports.MapsActivity.class);
			startActivity(intent);
			break;
		}
	}

}
