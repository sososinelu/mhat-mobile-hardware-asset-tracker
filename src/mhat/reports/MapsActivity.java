package mhat.reports;

import mhat.main.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class MapsActivity extends MapActivity {
	private MapView mv;
	GeoPoint p;

	class MapOverlay extends com.google.android.maps.Overlay {
		@Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
				long when) {
			super.draw(canvas, mapView, shadow);

			// ---translate the GeoPoint to screen pixels---
			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);

			// ---add the marker---
			Bitmap bmp = BitmapFactory.decodeResource(getResources(),
					R.drawable.map_marker);
			canvas.drawBitmap(bmp, screenPts.x, screenPts.y - 50, null);
			return true;
		}
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		mv = (MapView) findViewById(R.id.googleMapView);

		mv.getController().setCenter(getPoint(51.4936836, -3.178335575));
		mv.getController().setZoom(17);
		mv.setBuiltInZoomControls(true);

		/*
		 * MapOverlay mapOverlay = new MapOverlay(); List<Overlay>
		 * listOfOverlays = mv.getOverlays(); listOfOverlays.clear();
		 * listOfOverlays.add(mapOverlay);
		 * 
		 * mv.invalidate();
		 */

	}

	private GeoPoint getPoint(double lat, double lon) {
		return (new GeoPoint((int) (lat * 1000000.0), (int) (lon * 1000000)));
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
