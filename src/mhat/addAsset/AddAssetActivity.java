package mhat.addAsset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mhat.main.R;
import mhat.sqlite.Asset;
import mhat.sqlite.Branch;
import mhat.sqlite.DatabaseHandler;
import mhat.sqlite.Employee;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class AddAssetActivity extends TabActivity {
	private ImageView imageView;
	private TextView text_view;
	Spinner spinner;
	private ArrayList<String> branchName;
	private ArrayList<String> employeeName;
	private ArrayAdapter<String> spinnerBranchArrayAdapter;
	private ArrayAdapter<String> spinnerBranchArrayAdapter1;
	private ArrayAdapter<String> spinnerEmployeeArrayAdapter;
	private ArrayAdapter<String> spinnerEmployeeArrayAdapter1;
	String imageName;
	private String passNo;
	private File image;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add);
		tabsLayout();
		editAsset();

		final EditText editText2 = (EditText) findViewById(R.id.assetNoTextBoxAdd);
		editText2.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// hide the keyboard (not functioning properly)
				InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(editText2.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		});

		// Set branch name spinner
		spinnerBranchArray();
		spinnerBranchArrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, branchName);
		spinnerBranchArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(spinnerBranchArrayAdapter);
		spinnerBranchArrayAdapter.notifyDataSetChanged();

		// Set employee name spinner
		spinnerEmployeeArray();
		spinnerEmployeeArrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, employeeName);
		spinnerEmployeeArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(spinnerEmployeeArrayAdapter);
		spinnerEmployeeArrayAdapter.notifyDataSetChanged();
	}

	// This method is called at button click because we
	// assigned the name of the to the "On Click property" of the button

	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.barcodeButtonAdd:
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"QR_CODE_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"ONE_D_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"PRODUCT_MODE");
			startActivityForResult(intent, 0);
			break;

		case R.id.cameraButton:
			Intent cameraIntent = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(cameraIntent, 1);
			break;

		case R.id.assetCalendarButton:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(
					AddAssetActivity.this, odsl, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
			break;

		case R.id.puchaseCalendarButton:
			Calendar cal1 = Calendar.getInstance();
			DatePickerDialog datePickDiag1 = new DatePickerDialog(
					AddAssetActivity.this, odsl1, cal1.get(Calendar.YEAR),
					cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH));
			datePickDiag1.show();
			break;

		case R.id.warrantyCalendarButton:
			Calendar cal2 = Calendar.getInstance();
			DatePickerDialog datePickDiag2 = new DatePickerDialog(
					AddAssetActivity.this, odsl2, cal2.get(Calendar.YEAR),
					cal2.get(Calendar.MONTH), cal2.get(Calendar.DAY_OF_MONTH));
			datePickDiag2.show();
			break;

		case R.id.addBranchButton:
			Intent intent2 = new Intent(AddAssetActivity.this, AddBranch.class);
			startActivityForResult(intent2, 2);
			break;

		case R.id.addEmployeeButton:
			// Create out AlertDialog
			final Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Add Employee");
			builder.setCancelable(true);
			final EditText input = new EditText(this);
			input.setHint("Employee Name");
			builder.setView(input);
			builder.setPositiveButton("Add Employee",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							if (input.getText().toString().equals("")) {
								Toast.makeText(getApplicationContext(),
										"Enter employee name!",
										Toast.LENGTH_SHORT).show();
							} else {
								// Add new employee to database
								DatabaseHandler db = new DatabaseHandler(
										AddAssetActivity.this);
								String name = input.getText().toString();

								// Add employee to the database
								db.addEmployee(new Employee(name));
								Toast.makeText(getApplicationContext(),
										"New Employee Saved" + "",
										Toast.LENGTH_SHORT).show();

								// Reset spinner adapter to display the new
								// employee
								spinnerEmployeeAdapterReset();
							}
						}
					});
			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Close the alert dialog
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
			break;

		case R.id.saveButton:
			DatabaseHandler db = new DatabaseHandler(this);

			// General tab
			EditText asset_no = (EditText) findViewById(R.id.assetNoTextBoxAdd);
			EditText asset_description = (EditText) findViewById(R.id.editTextDescription);
			EditText asset_name = (EditText) findViewById(R.id.editTextName);
			EditText asset_manufacturer = (EditText) findViewById(R.id.editTextManufacturer);
			EditText asset_model = (EditText) findViewById(R.id.editTextModel);
			EditText serial_no = (EditText) findViewById(R.id.editTextSerialNo);
			Spinner asset_status = (Spinner) findViewById(R.id.spinnerStatus);
			TextView audit_date = (TextView) findViewById(R.id.labelAuditDateSet);
			// Purchase tab
			EditText asset_supplier = (EditText) findViewById(R.id.editTextSupplier);
			EditText invoice_no = (EditText) findViewById(R.id.editTextInvoiceNo);
			TextView purchase_date = (TextView) findViewById(R.id.labelPurchaseDateSet);
			TextView warranty_date = (TextView) findViewById(R.id.labelWarrantyDateSet);
			EditText warranty_no = (EditText) findViewById(R.id.editTextWarrantyNo);
			EditText warranty_details = (EditText) findViewById(R.id.editTextWarrantyDetails);
			// Location tab
			Spinner asset_department = (Spinner) findViewById(R.id.spinnerDepartment);
			Spinner asset_employee = (Spinner) findViewById(R.id.spinnerEmployee);
			Spinner asset_branch = (Spinner) findViewById(R.id.spinnerBranch);

			if (asset_no.getText().toString().equals("")
					|| asset_description.getText().toString().equals("")
					|| asset_name.getText().toString().equals("")) {

				Toast.makeText(getApplicationContext(),
						"Enter all asset details!", Toast.LENGTH_SHORT).show();
			} else {
				// Save the image to the SD card
				imageView = (ImageView) findViewById(R.id.cameraButton);
				BitmapDrawable drawable = (BitmapDrawable) imageView
						.getDrawable();
				Bitmap bitmap = drawable.getBitmap();
				String timestamp = String.valueOf(System.currentTimeMillis());
				// Path to the image folder
				File sdCardDirectory = new File(Environment
						.getExternalStorageDirectory().toString()
						+ "/MHAT/Images");
				sdCardDirectory.mkdirs();
				imageName = "Asset." + timestamp + ".png";
				// Image file path and name
				image = new File(sdCardDirectory, imageName);

				boolean success = false;

				FileOutputStream outStream;

				try {
					// Encode the file as a PNG image
					outStream = new FileOutputStream(image);
					// 100 to keep full quality of the image
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
					// Close the file output stream
					outStream.flush();
					outStream.close();
					success = true;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (success) {
					Toast.makeText(getApplicationContext(),
							"Image saved with success", Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(getApplicationContext(),
							"Error during image saving", Toast.LENGTH_SHORT)
							.show();
				}

				// Save asset to the database

				db.addAsset(new Asset(asset_no.getText().toString(),
						asset_description.getText().toString(), asset_name
								.getText().toString(), asset_manufacturer
								.getText().toString(), asset_model.getText()
								.toString(), serial_no.getText().toString(),
						asset_status.getItemAtPosition(
								asset_status.getSelectedItemPosition())
								.toString(), audit_date.getText().toString(),
						asset_supplier.getText().toString(), invoice_no
								.getText().toString(), purchase_date.getText()
								.toString(),
						warranty_date.getText().toString(), warranty_no
								.getText().toString(), warranty_details
								.getText().toString(), asset_department
								.getItemAtPosition(
										asset_department
												.getSelectedItemPosition())
								.toString(), asset_employee.getItemAtPosition(
								asset_employee.getSelectedItemPosition())
								.toString(), asset_branch.getItemAtPosition(
								asset_branch.getSelectedItemPosition())
								.toString(), imageName));

				finish();
				Toast.makeText(getApplicationContext(), "New Asset Saved",
						Toast.LENGTH_SHORT).show();
			}
			break;

		// spinner.
		// fields.getItemAtPosition(fields.getSelectedItemPosition()).toString());

		case R.id.cancelButton:
			finish();
			Toast.makeText(getApplicationContext(),
					"The new Asset was cancelled!", Toast.LENGTH_SHORT).show();
			break;

		case R.id.deleteDatabase:
			DatabaseHandler db1 = new DatabaseHandler(this);
			// db1.deleteAllAssets();
			db1.deleteAllBranch();
			db1.deleteAllEmployees();

			finish();
			Toast.makeText(getApplicationContext(), "Database deleted!",
					Toast.LENGTH_SHORT).show();
			break;
		}
	}

	public void tabsLayout() {
		Resources res = getResources();
		TabHost tabHost = getTabHost();

		View tab1View = createTabView(tabHost.getContext(), "General",
				res.getDrawable(R.drawable.laptop));

		View tab2View = createTabView(tabHost.getContext(), "Purchase",
				res.getDrawable(R.drawable.purchase));

		View tab3View = createTabView(tabHost.getContext(), "Location",
				res.getDrawable(R.drawable.location));

		View tab4View = createTabView(tabHost.getContext(), "Save",
				res.getDrawable(R.drawable.save));

		tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(tab1View)
				.setContent(R.id.tab1layout));

		tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(tab2View)
				.setContent(R.id.tab2layout));

		tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(tab3View)
				.setContent(R.id.tab3layout));

		tabHost.addTab(tabHost.newTabSpec("tab4").setIndicator(tab4View)
				.setContent(R.id.tab4layout));

		tabHost.setCurrentTab(0);
	}

	private static View createTabView(final Context context, final String text,
			final Drawable drawable) {

		View view = LayoutInflater.from(context).inflate(R.layout.tabs_design,
				null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		ImageView iv = (ImageView) view.findViewById(R.id.tabsIcon);
		iv.setBackgroundDrawable(drawable);
		tv.setText(text);
		return view;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				// Handle successful scan and display contents
				text_view = (TextView) findViewById(R.id.assetNoTextBoxAdd);
				text_view.setText(contents);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}

		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				Bitmap photo = (Bitmap) intent.getExtras().get("data");
				imageView = (ImageView) findViewById(R.id.cameraButton);
				imageView.setImageBitmap(photo);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}

		if (requestCode == 2) {
			if (resultCode == RESULT_OK) {
				spinnerBranchArrayAdapter.clear();
				spinnerBranchArray();
				spinnerBranchArrayAdapter1 = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, branchName);
				spinnerBranchArrayAdapter1
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinner.setAdapter(spinnerBranchArrayAdapter1);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	final OnDateSetListener odsl = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month,
				int dayOfMonth) {
			month++;
			TextView txt = (TextView) findViewById(R.id.labelAuditDateSet);
			txt.setText(dayOfMonth + "/" + month + "/" + year);
		}
	};

	final OnDateSetListener odsl1 = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month,
				int dayOfMonth) {
			month++;
			TextView txt = (TextView) findViewById(R.id.labelPurchaseDateSet);
			txt.setText(dayOfMonth + "/" + month + "/" + year);
		}
	};

	final OnDateSetListener odsl2 = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month,
				int dayOfMonth) {
			month++;
			TextView txt = (TextView) findViewById(R.id.labelWarrantyDateSet);
			txt.setText(dayOfMonth + "/" + month + "/" + year);
		}
	};

	public void spinnerBranchArray() {
		DatabaseHandler db = new DatabaseHandler(this);
		spinner = (Spinner) findViewById(R.id.spinnerBranch);

		List<Branch> branch = db.getAllBranches();
		branchName = new ArrayList<String>();

		for (Branch bn : branch) {
			String log = bn.getBranchName();
			branchName.add(log);
		}
	}

	public void spinnerEmployeeArray() {
		DatabaseHandler db = new DatabaseHandler(this);
		spinner = (Spinner) findViewById(R.id.spinnerEmployee);

		List<Employee> employee = db.getAllEmployees();
		employeeName = new ArrayList<String>();

		for (Employee en : employee) {
			String log = en.getEmployeeName();
			employeeName.add(log);
		}
	}

	public void spinnerEmployeeAdapterReset() {
		spinnerEmployeeArrayAdapter.clear();
		spinnerEmployeeArray();
		spinnerEmployeeArrayAdapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, employeeName);
		spinnerEmployeeArrayAdapter1
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerEmployeeArrayAdapter1);
	}

	public void editAsset() {
		/*
		 * Intent from the All Asset Activity containing the assetID
		 */

		Intent intent1 = getIntent();
		// Retrieve variable from intent
		passNo = intent1.getStringExtra("strings");
		// Set String variable to search box
		/*
		 * TextView test = (TextView) findViewById(R.id.test);
		 * test.setText(passNo);
		 * 
		 * if (passNo.equals("") == false) {
		 * 
		 * DatabaseHandler db = new DatabaseHandler(this); Asset asset =
		 * db.getAsset(Integer.parseInt(passNo));
		 * 
		 * EditText asset_no = (EditText) findViewById(R.id.assetNoTextBoxAdd);
		 * asset_no.setText(asset.getAssetNo()); }
		 */

		/*
		 * asset_description.setText(asset.getDescription());
		 * asset_name.setText(asset.getName());
		 * asset_manufacturer.setText(asset.getManufacturer());
		 * asset_model.setText(asset.getModel());
		 * serial_no.setText(asset.getSerialNo());
		 * asset_status.setTag(asset.getAssetStatus());
		 * audit_date.setText(asset.getAuditDate());
		 * asset_supplier.setText(asset.getSupplier());
		 * invoice_no.setText(asset.getInvoiceNo());
		 * purchase_date.setText(asset.getPurchaseDate());
		 * warranty_date.setText(asset.getWarrantyExpire());
		 * warranty_no.setText(asset.getWarrantyNo());
		 * warranty_details.setText(asset.getWarrantyDetails());
		 * asset_department.setTag(asset.getDepartment());
		 * asset_employee.setTag(asset.getEmployee());
		 * asset_branch.setTag(asset.getBranch());
		 * 
		 * String imageName = asset.getPictureName();
		 */

	}

	// Create menu options
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_menu, menu);
		return true;
	}

	// This method is called once the menu is selected
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Menu options
		case R.id.homeMenu:
			Intent intent = new Intent(AddAssetActivity.this,
					mhat.main.MainActivity.class);
			startActivity(intent);
			break;
		}
		return true;
	}
}