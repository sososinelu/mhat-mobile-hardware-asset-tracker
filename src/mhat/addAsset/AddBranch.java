package mhat.addAsset;

import mhat.main.R;
import mhat.sqlite.Branch;
import mhat.sqlite.DatabaseHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class AddBranch extends Activity implements LocationListener {
	private String tag = "Main"; // For Log
	private LocationManager lm;
	private TextView setCoord;
	private EditText branch_latitude;
	private EditText branch_longitude;
	private StringBuilder sb;
	private String provider;
	private int noOfFixes = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_branch);
		location();
	}

	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.buttonAddBranch:
			DatabaseHandler db = new DatabaseHandler(this);

			EditText branch_name = (EditText) findViewById(R.id.editTextBranchName);
			EditText branch_latitude = (EditText) findViewById(R.id.editTextBranchLatitude);
			EditText branch_longitude = (EditText) findViewById(R.id.editTextBranchLongitude);

			if (branch_name.getText().toString().equals("")
					|| branch_latitude.getText().toString().equals("")
					|| branch_longitude.getText().toString().equals("")) {

				Toast.makeText(getApplicationContext(),
						"Enter branch details!", Toast.LENGTH_LONG).show();
			} else {
				// Add branch to the database
				db.addBranch(new Branch(
						branch_name.getText().toString(),
						Double.parseDouble(branch_latitude.getText().toString()),
						Double.parseDouble(branch_longitude.getText()
								.toString())));

				// Pass the new branch to the AddAssetActivity
				Intent intent = new Intent(AddBranch.this,
						AddAssetActivity.class);
				// startActivity(intent);
				this.setResult(RESULT_OK, intent);
				finish();
				Toast.makeText(getApplicationContext(), "New Branch Saved.",
						Toast.LENGTH_LONG).show();
			}

			break;

		case R.id.butonAddBranchCancel:
			finish();
			break;

		case R.id.findLocationButton:
			/* This is called if/when the GPS is disabled in settings */
			Log.v(tag, "Disabled");
			ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
			// Create out AlertDialog
			Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("GPS Disabled");
			builder.setMessage("Enable GPS ?");
			builder.setCancelable(true);
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Bring up the GPS settings
							Intent intent = new Intent(
									android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(intent);
						}
					});
			builder.setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Toast.makeText(getApplicationContext(),
									"GPS Provider Disabled", Toast.LENGTH_LONG)
									.show();
							ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
							progressBar.setVisibility(View.GONE);
						}
					});
			AlertDialog dialog = builder.create();
			if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				dialog.show();
				progressBar.setVisibility(View.VISIBLE);
			}
			break;

		case R.id.lastLocationButton:

			lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			provider = lm.getBestProvider(criteria, false);
			Location location = lm.getLastKnownLocation(provider);

			// Set text fields with latitude & longitude info
			branch_latitude = (EditText) findViewById(R.id.editTextBranchLatitude);
			branch_longitude = (EditText) findViewById(R.id.editTextBranchLongitude);

			branch_latitude.setText(Double.toString(location.getLatitude()));
			branch_longitude.setText(Double.toString(location.getLongitude()));

			break;
		}
	}

	public void location() {
		setCoord = (TextView) findViewById(R.id.labelSetCoordinates);
		/*
		 * the location manager is the most vital part it allows access to
		 * location and GPS status services
		 */
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		provider = lm.getBestProvider(criteria, false);
		Location location = lm.getLastKnownLocation(provider);

		// Initialise the location fields
		if (location != null) {
			sb = new StringBuilder(512);

			noOfFixes++;

			sb.append("Last known location: ");
			sb.append('\n');
			sb.append('\n');

			sb.append("No. of Fixes: ");
			sb.append(noOfFixes);
			sb.append('\n');
			sb.append('\n');

			sb.append("Latitude:   ");
			sb.append(location.getLatitude());
			sb.append('\n');

			sb.append("Longitude: ");
			sb.append(location.getLongitude());

			setCoord.setText(sb.toString());
		}
	}

	@Override
	protected void onResume() {
		/*
		 * onResume is is always called after onStart, even if the app hasn't
		 * been paused
		 * 
		 * add location listener and request updates every 120000ms (2min) or
		 * 10m
		 */
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 120000, 10f,
				this, null);
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

		if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			progressBar.setVisibility(View.GONE);
		} else {
			progressBar.setVisibility(View.VISIBLE);
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		/* GPS, as it turns out, consumes battery like crazy */
		lm.removeUpdates(this);
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
		super.onPause();
	}

	@Override
	public void onLocationChanged(Location location) {
		setCoord = (TextView) findViewById(R.id.labelSetCoordinates);
		Log.v(tag, "Location Changed");

		sb = new StringBuilder(512);

		noOfFixes++;

		/* Display some of the data in the TextView */

		sb.append("No. of Fixes: ");
		sb.append(noOfFixes);
		sb.append('\n');
		sb.append('\n');

		sb.append("Latitude:   ");
		sb.append(location.getLatitude());
		sb.append('\n');

		sb.append("Longitude: ");
		sb.append(location.getLongitude());

		setCoord.setText(sb.toString());
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);

		// Set text fields with latitude & longitude info
		branch_latitude = (EditText) findViewById(R.id.editTextBranchLatitude);
		branch_longitude = (EditText) findViewById(R.id.editTextBranchLongitude);

		branch_latitude.setText(Double.toString(location.getLatitude()));
		branch_longitude.setText(Double.toString(location.getLongitude()));
	}

	@Override
	public void onProviderDisabled(String provider) {
		// Toast.makeText(this, "GPS Provider Disabled", Toast.LENGTH_SHORT)
		// .show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.v(tag, "Enabled");
		Toast.makeText(this, "GPS Provider Enabled", Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		/* This is called when the GPS status alters */
		switch (status) {
		case LocationProvider.OUT_OF_SERVICE:
			Log.v(tag, "Status Changed: Out of Service");
			Toast.makeText(this, "Status Changed: Out of Service",
					Toast.LENGTH_SHORT).show();
			break;
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
			Log.v(tag, "Status Changed: Temporarily Unavailable");
			Toast.makeText(this, "Status Changed: Temporarily Unavailable",
					Toast.LENGTH_SHORT).show();
			break;
		case LocationProvider.AVAILABLE:
			Log.v(tag, "Status Changed: Available");
			Toast.makeText(this, "Status Changed: Available",
					Toast.LENGTH_SHORT).show();
			break;
		}

	}
}
