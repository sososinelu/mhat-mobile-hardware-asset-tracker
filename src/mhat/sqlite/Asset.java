package mhat.sqlite;

public class Asset {
	// Private variables
	int id;
	String asset_no;
	String description;
	String name;
	String manufacturer;
	String model;
	String serial_no;
	String asset_status;
	String audit_date;
	String supplier;
	String invoice_no;
	String purchase_date;
	String warranty_expire;
	String warranty_no;
	String warranty_details;
	String department;
	String employee;
	String branch;
	String picture_name;

	// Empty constructor
	public Asset() {
	}

	// Constructor
	public Asset(int id, String asset_no, String description, String name,
			String manufacturer, String model, String serial_no,
			String asset_status, String audit_date, String supplier,
			String invoice_no, String purchase_date, String warranty_expire,
			String warranty_no, String warranty_details, String department,
			String employee, String branch, String picture_name) {

		this.id = id;
		this.asset_no = asset_no;
		this.description = description;
		this.name = name;
		this.manufacturer = manufacturer;
		this.model = model;
		this.serial_no = serial_no;
		this.asset_status = asset_status;
		this.audit_date = audit_date;
		this.supplier = supplier;
		this.invoice_no = invoice_no;
		this.purchase_date = purchase_date;
		this.warranty_expire = warranty_expire;
		this.warranty_no = warranty_no;
		this.warranty_details = warranty_details;
		this.department = department;
		this.employee = employee;
		this.branch = branch;
		this.picture_name = picture_name;
	}

	// Constructor
	public Asset(String asset_no, String description, String name,
			String manufacturer, String model, String serial_no,
			String asset_status, String audit_date, String supplier,
			String invoice_no, String purchase_date, String warranty_expire,
			String warranty_no, String warranty_details, String department,
			String employee, String branch, String picture_name) {

		this.asset_no = asset_no;
		this.description = description;
		this.name = name;
		this.manufacturer = manufacturer;
		this.model = model;
		this.serial_no = serial_no;
		this.asset_status = asset_status;
		this.audit_date = audit_date;
		this.supplier = supplier;
		this.invoice_no = invoice_no;
		this.purchase_date = purchase_date;
		this.warranty_expire = warranty_expire;
		this.warranty_no = warranty_no;
		this.warranty_details = warranty_details;
		this.department = department;
		this.employee = employee;
		this.branch = branch;
		this.picture_name = picture_name;
	}

	/*
	 * // Constructor public Asset(int id, String audit_date) {
	 * 
	 * this.id = id; this.audit_date = audit_date; }
	 * 
	 * // Constructor public Asset(String audit_date) { this.audit_date =
	 * audit_date; }
	 */

	// Getters & setters

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public String getAssetNo() {
		return asset_no;
	}

	public void setAssetNo(String asset_no) {
		this.asset_no = asset_no;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNo() {
		return serial_no;
	}

	public void setSerialNo(String serial_no) {
		this.serial_no = serial_no;
	}

	public String getAssetStatus() {
		return asset_status;
	}

	public void setAssetStatus(String asset_status) {
		this.asset_status = asset_status;
	}

	public String getAuditDate() {
		return audit_date;
	}

	public void setAuditDate(String audit_date) {
		this.audit_date = audit_date;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getInvoiceNo() {
		return invoice_no;
	}

	public void setInvoiceNo(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public String getPurchaseDate() {
		return purchase_date;
	}

	public void setPurchaseDate(String purchase_date) {
		this.purchase_date = purchase_date;
	}

	public String getWarrantyExpire() {
		return warranty_expire;
	}

	public void setWarrantyExpire(String warranty_expire) {
		this.warranty_expire = warranty_expire;
	}

	public String getWarrantyNo() {
		return warranty_no;
	}

	public void setWarrantyNo(String warranty_no) {
		this.warranty_no = warranty_no;
	}

	public String getWarrantyDetails() {
		return warranty_details;
	}

	public void setWarrantyDetails(String warranty_details) {
		this.warranty_details = warranty_details;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getPictureName() {
		return picture_name;
	}

	public void setPictureName(String picture_name) {
		this.picture_name = picture_name;
	}
}
