package mhat.sqlite;

public class Branch {
	// Private variables
	int id;
	String branch_name;
	double latitude;
	double longitude;

	// Empty constructor
	public Branch() {
	}

	// Constructor
	public Branch(int id, String branch_name, double latitude, double longitude) {
		this.id = id;
		this.branch_name = branch_name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	// Constructor
	public Branch(String branch_name, double latitude, double longitude) {
		this.branch_name = branch_name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	// Getting ID
	public int getID() {
		return this.id;
	}

	// Setting ID
	public void setID(int id) {
		this.id = id;
	}

	// Getting branch name
	public String getBranchName() {
		return this.branch_name;
	}

	// Setting branch name
	public void setBranchName(String branch_name) {
		this.branch_name = branch_name;
	}

	// Getting latitude
	public double getLatitude() {
		return this.latitude;
	}

	// Setting latitude
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	// Getting longitude
	public double getLongitude() {
		return longitude;
	}

	// Setting longitude
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
