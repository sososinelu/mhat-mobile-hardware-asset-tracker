package mhat.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	/*
	 * All Static variables
	 */

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "MHAT";

	/*
	 * Branch table variables
	 */

	// Branch table name
	private static final String TABLE_BRANCH = "branch";

	// Branch table columns names
	private static final String KEY_ID = "id";
	private static final String KEY_BRANCH_NAME = "branch_name";
	private static final String KEY_LATITUDE = "latitude";
	private static final String KEY_LONGITUDE = "longitude";

	/*
	 * Employee table variables
	 */

	// Employee table name
	private static final String TABLE_EMPLOYEE = "employee";

	// Employee table columns names
	private static final String KEY_ID_E = "id";
	private static final String KEY_EMPLOYEE_NAME = "employee_name";

	/*
	 * Asset table variables
	 */

	// Asset table name
	private static final String TABLE_ASSET = "asset";

	// Asset table columns names
	private static final String KEY_ID_A = "id";
	private static final String KEY_ASSET_NO = "asset_no";
	private static final String KEY_DESCRIPTION = "description";
	private static final String KEY_NAME = "name";
	private static final String KEY_MANUFACTURER = "manufacturer";
	private static final String KEY_MODEL = "model";
	private static final String KEY_SERIAL_NO = "serial_no";
	private static final String KEY_ASSET_STATUS = "asset_status";
	private static final String KEY_AUDIT_DATE = "audit_date";
	private static final String KEY_SUPPLIER = "supplier";
	private static final String KEY_INVOICE_NO = "invoice_no";
	private static final String KEY_PURCHASE_DATE = "purchase_date";
	private static final String KEY_WARRANTY_EXPIRE = "warranty_expire";
	private static final String KEY_WARRANTY_NO = "warranty_no";
	private static final String KEY_WARRANTY_DETAILS = "warranty_details";
	private static final String KEY_DEPARTMENT = "department";
	private static final String KEY_EMPLOYEE = "employee";
	private static final String KEY_BRANCH = "branch";
	private static final String KEY_PICTURE_PATH = "picture_path";

	// Database handler
	private SQLiteDatabase db;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		// Create Branch table
		String CREATE_BRANCH_TABLE = "CREATE TABLE " + TABLE_BRANCH + "("
				+ KEY_ID + " INTEGER PRIMARY KEY, " + KEY_BRANCH_NAME
				+ " TEXT, " + KEY_LATITUDE + " NUMERIC, " + KEY_LONGITUDE
				+ " NUMERIC" + ")";
		db.execSQL(CREATE_BRANCH_TABLE);

		// Create Employee table
		String CREATE_EMPLOYEE_TABLE = "CREATE TABLE " + TABLE_EMPLOYEE + "("
				+ KEY_ID_E + " INTEGER PRIMARY KEY, " + KEY_EMPLOYEE_NAME
				+ " TEXT" + ")";
		db.execSQL(CREATE_EMPLOYEE_TABLE);

		// Create Asset table
		String CREATE_ASSET_TABLE = "CREATE TABLE " + TABLE_ASSET + "("
				+ KEY_ID_A + " INTEGER PRIMARY KEY, " + KEY_ASSET_NO
				+ " TEXT, " + KEY_DESCRIPTION + " TEXT, " + KEY_NAME
				+ " TEXT, " + KEY_MANUFACTURER + " TEXT, " + KEY_MODEL
				+ " TEXT, " + KEY_SERIAL_NO + " TEXT, " + KEY_ASSET_STATUS
				+ " TEXT, " + KEY_AUDIT_DATE + " TEXT, " + KEY_SUPPLIER
				+ " TEXT, " + KEY_INVOICE_NO + " TEXT, " + KEY_PURCHASE_DATE
				+ " TEXT, " + KEY_WARRANTY_EXPIRE + " TEXT, " + KEY_WARRANTY_NO
				+ " TEXT, " + KEY_WARRANTY_DETAILS + " TEXT, " + KEY_DEPARTMENT
				+ " TEXT, " + KEY_EMPLOYEE + " TEXT, " + KEY_BRANCH + " TEXT, "
				+ KEY_PICTURE_PATH + " TEXT" + ")";
		db.execSQL(CREATE_ASSET_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_BRANCH + TABLE_EMPLOYEE
				+ TABLE_ASSET);

		// Create tables again
		onCreate(db);
	}

	/*
	 * Branch table methods
	 */

	// Adding a new branch
	public void addBranch(Branch branch) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_BRANCH_NAME, branch.getBranchName()); // Branch name
		values.put(KEY_LATITUDE, branch.getLatitude()); // Branch latitude
		values.put(KEY_LONGITUDE, branch.getLongitude()); // Branch longitude

		// Inserting row
		db.insert(TABLE_BRANCH, null, values);
		db.close(); // Closing database connection
	}

	// Getting single branch
	public Branch getBranch(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_BRANCH, new String[] { KEY_ID,
				KEY_BRANCH_NAME, KEY_LATITUDE, KEY_LONGITUDE }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Branch branch = new Branch(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), Double.parseDouble(cursor.getString(2)),
				Double.parseDouble(cursor.getString(3)));
		// Return branch
		return branch;
	}

	// Getting all branches
	public List<Branch> getAllBranches() {
		List<Branch> branchList = new ArrayList<Branch>();
		// Select all query
		String selectQuery = "SELECT * FROM " + TABLE_BRANCH;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// Looping trough all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Branch branch = new Branch();
				branch.setID(Integer.parseInt(cursor.getString(0)));
				branch.setBranchName(cursor.getString(1));
				branch.setLatitude(Double.parseDouble(cursor.getString(2)));
				branch.setLongitude(Double.parseDouble(cursor.getString(3)));
				// Adding branch to the list
				branchList.add(branch);
			} while (cursor.moveToNext());
		}

		db.close(); // Closing database connection

		// Return branch list
		return branchList;
	}

	// Getting branches count
	public int getBranchesCount() {
		String countQuerry = "SELECT * FROM " + TABLE_BRANCH;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuerry, null);
		cursor.close();

		// Return count
		return cursor.getCount();
	}

	// Updating single branch
	public int updateBranch(Branch branch) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_BRANCH_NAME, branch.getBranchName());
		values.put(KEY_LATITUDE, branch.getLatitude());
		values.put(KEY_LONGITUDE, branch.getLongitude());

		db.close(); // Closing database connection
		// Updating row
		return db.update(TABLE_BRANCH, values, KEY_ID + " =?",
				new String[] { String.valueOf(branch.getID()) });
	}

	// Deleting single branch
	public void deleteBranch(Branch branch) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_BRANCH, KEY_ID + " = ?",
				new String[] { String.valueOf(branch.getID()) });
		db.close();
	}

	// Delete all branches
	public void deleteAllBranch() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_BRANCH, null, null);
		db.close();
	}

	/*
	 * Employee table methods
	 */

	// Adding a new employee
	public void addEmployee(Employee employee) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_EMPLOYEE_NAME, employee.getEmployeeName()); // Branch
																	// name
		// Inserting row
		db.insert(TABLE_EMPLOYEE, null, values);
		db.close(); // Closing database connection
	}

	// Getting all employees
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = new ArrayList<Employee>();
		// Select all query
		String selectQuery = "SELECT * FROM " + TABLE_EMPLOYEE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// Looping trough all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Employee employee = new Employee();
				employee.setID(Integer.parseInt(cursor.getString(0)));
				employee.setEmployeeName(cursor.getString(1));
				// Adding employee to the list
				employeeList.add(employee);
			} while (cursor.moveToNext());
		}

		db.close(); // Closing database connection

		// Return employee list
		return employeeList;
	}

	// Getting employees count
	public int getEmployeeCount() {
		String countQuerry = "SELECT * FROM " + TABLE_EMPLOYEE;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuerry, null);
		cursor.close();

		// Return count
		return cursor.getCount();
	}

	// Delete all employees
	public void deleteAllEmployees() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_EMPLOYEE, null, null);
		db.close();
	}

	/*
	 * Asset table methods
	 */

	// Adding a new asset
	public void addAsset(Asset asset) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ASSET_NO, asset.getAssetNo()); // Asset no
		values.put(KEY_DESCRIPTION, asset.getDescription()); // Asset
																// description
		values.put(KEY_NAME, asset.getName()); // Asset name
		values.put(KEY_MANUFACTURER, asset.getManufacturer()); // Asset
																// manufacturer
		values.put(KEY_MODEL, asset.getModel()); // Asset model
		values.put(KEY_SERIAL_NO, asset.getSerialNo()); // Asset serial no
		values.put(KEY_ASSET_STATUS, asset.getAssetStatus()); // Asset status
		values.put(KEY_AUDIT_DATE, asset.getAuditDate()); // Asset audit date
		values.put(KEY_SUPPLIER, asset.getSupplier()); // Asset supplier
		values.put(KEY_INVOICE_NO, asset.getInvoiceNo()); // Asset invoice no
		values.put(KEY_PURCHASE_DATE, asset.getPurchaseDate()); // Asset
																// purchase date
		values.put(KEY_WARRANTY_EXPIRE, asset.getWarrantyExpire()); // Asset
																	// warranty
																	// expire
																	// date
		values.put(KEY_WARRANTY_NO, asset.getWarrantyNo()); // Asset warranty no
		values.put(KEY_WARRANTY_DETAILS, asset.getWarrantyDetails()); // Asset
																		// warranty
																		// details
		values.put(KEY_DEPARTMENT, asset.getDepartment()); // Asset department
		values.put(KEY_EMPLOYEE, asset.getEmployee()); // Asset employee
		values.put(KEY_BRANCH, asset.getBranch()); // // Asset branch
		values.put(KEY_PICTURE_PATH, asset.getPictureName()); // Asset picture

		// Inserting row
		db.insert(TABLE_ASSET, null, values);
		db.close(); // Closing database connection
	}

	// Getting single asset
	public Asset getAsset(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_ASSET, new String[] { KEY_ID_A,
				KEY_ASSET_NO, KEY_DESCRIPTION, KEY_NAME, KEY_MANUFACTURER,
				KEY_MODEL, KEY_SERIAL_NO, KEY_ASSET_STATUS, KEY_AUDIT_DATE,
				KEY_SUPPLIER, KEY_INVOICE_NO, KEY_PURCHASE_DATE,
				KEY_WARRANTY_EXPIRE, KEY_WARRANTY_NO, KEY_WARRANTY_DETAILS,
				KEY_DEPARTMENT, KEY_EMPLOYEE, KEY_BRANCH, KEY_PICTURE_PATH },
				KEY_ID_A + "=?", new String[] { String.valueOf(id) }, null,
				null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Asset asset = new Asset(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5), cursor.getString(6),
				cursor.getString(7), cursor.getString(8), cursor.getString(9),
				cursor.getString(10), cursor.getString(11),
				cursor.getString(12), cursor.getString(13),
				cursor.getString(14), cursor.getString(15),
				cursor.getString(16), cursor.getString(17),
				cursor.getString(18));

		// Return asset
		return asset;
	}

	// Getting single asset
	public Asset getAssetNo(String no) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_ASSET, new String[] { KEY_ID_A,
				KEY_ASSET_NO, KEY_DESCRIPTION, KEY_NAME, KEY_MANUFACTURER,
				KEY_MODEL, KEY_SERIAL_NO, KEY_ASSET_STATUS, KEY_AUDIT_DATE,
				KEY_SUPPLIER, KEY_INVOICE_NO, KEY_PURCHASE_DATE,
				KEY_WARRANTY_EXPIRE, KEY_WARRANTY_NO, KEY_WARRANTY_DETAILS,
				KEY_DEPARTMENT, KEY_EMPLOYEE, KEY_BRANCH, KEY_PICTURE_PATH },
				KEY_ID_A + "=?", new String[] { String.valueOf(no) }, null,
				null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Asset asset = new Asset(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5), cursor.getString(6),
				cursor.getString(7), cursor.getString(8), cursor.getString(9),
				cursor.getString(10), cursor.getString(11),
				cursor.getString(12), cursor.getString(13),
				cursor.getString(14), cursor.getString(15),
				cursor.getString(16), cursor.getString(17),
				cursor.getString(18));

		// Return asset
		return asset;
	}

	// Getting all assets
	public List<Asset> getAllAssets() {
		List<Asset> assetList = new ArrayList<Asset>();
		// Select all query
		String selectQuery = "SELECT * FROM " + TABLE_ASSET;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// Looping trough all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Asset asset = new Asset();
				asset.setID(Integer.parseInt(cursor.getString(0)));
				asset.setAssetNo(cursor.getString(1));
				asset.setDescription(cursor.getString(2));
				asset.setName(cursor.getString(3));
				asset.setManufacturer(cursor.getString(4));
				asset.setModel(cursor.getString(5));
				asset.setSerialNo(cursor.getString(6));
				asset.setAssetStatus(cursor.getString(7));
				asset.setAuditDate(cursor.getString(8));
				asset.setSupplier(cursor.getString(9));
				asset.setInvoiceNo(cursor.getString(10));
				asset.setPurchaseDate(cursor.getString(11));
				asset.setWarrantyExpire(cursor.getString(12));
				asset.setWarrantyNo(cursor.getString(13));
				asset.setWarrantyDetails(cursor.getString(14));
				asset.setDepartment(cursor.getString(15));
				asset.setEmployee(cursor.getString(16));
				asset.setBranch(cursor.getString(17));
				asset.setPictureName(cursor.getString(18));
				// Adding asset to the list
				assetList.add(asset);
			} while (cursor.moveToNext());
		}

		db.close(); // Closing database connection

		// Return asset list
		return assetList;
	}

	// Getting branches count
	public int getAssetsCount() {
		String countQuerry = "SELECT * FROM " + TABLE_ASSET;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuerry, null);
		cursor.close();

		// Return count
		return cursor.getCount();
	}

	// Updating single asset
	public void updateAsset(Asset asset) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.CONFLICT_IGNORE:

		ContentValues values = new ContentValues();
		values.put(KEY_ASSET_NO, asset.getAssetNo());
		values.put(KEY_DESCRIPTION, asset.getDescription());
		values.put(KEY_NAME, asset.getName());
		values.put(KEY_MANUFACTURER, asset.getManufacturer());
		values.put(KEY_MODEL, asset.getModel());
		values.put(KEY_SERIAL_NO, asset.getSerialNo());
		values.put(KEY_ASSET_STATUS, asset.getAssetStatus());
		values.put(KEY_AUDIT_DATE, asset.getAuditDate());
		values.put(KEY_SUPPLIER, asset.getSupplier());
		values.put(KEY_INVOICE_NO, asset.getInvoiceNo());
		values.put(KEY_PURCHASE_DATE, asset.getPurchaseDate());
		values.put(KEY_WARRANTY_EXPIRE, asset.getWarrantyExpire());
		values.put(KEY_WARRANTY_NO, asset.getWarrantyNo());
		values.put(KEY_WARRANTY_DETAILS, asset.getWarrantyDetails());
		values.put(KEY_DEPARTMENT, asset.getDepartment());
		values.put(KEY_EMPLOYEE, asset.getEmployee());
		values.put(KEY_BRANCH, asset.getBranch());
		values.put(KEY_PICTURE_PATH, asset.getPictureName());

		// Updating row
		db.update(TABLE_ASSET, values, KEY_ID_A + " =?",
				new String[] { String.valueOf(asset.getID()) });
		db.close(); // Closing database connection

	}

	// Updating audit date asset
	public int updateAuditDate(Asset asset) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_AUDIT_DATE, asset.getAuditDate());

		db.close(); // Closing database connection

		// Updating row
		return db.update(TABLE_ASSET, values, KEY_ID_A + " =?",
				new String[] { String.valueOf(asset.getID()) });
	}

	// Deleting single asset
	public void deleteAsset(Asset asset) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ASSET, KEY_ID_A + " = ?",
				new String[] { String.valueOf(asset.getID()) });
		db.close();
	}

	// Delete all branches
	public void deleteAllAssets() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ASSET, null, null);
		db.close();
	}
}
