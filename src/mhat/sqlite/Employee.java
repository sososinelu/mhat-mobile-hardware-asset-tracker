package mhat.sqlite;

public class Employee {
	// Private variables
	private int id;
	private String employee_name;

	// Empty constructor
	public Employee() {
	}

	// Constructor
	public Employee(int id, String employee_name) {
		this.id = id;
		this.employee_name = employee_name;
	}

	// Constructor
	public Employee(String employee_name) {
		this.employee_name = employee_name;
	}

	// Getting ID
	public int getID() {
		return this.id;
	}

	// Setting ID
	public void setID(int id) {
		this.id = id;
	}

	// Getting employee name
	public String getEmployeeName() {
		return this.employee_name;
	}

	// Setting employee name
	public void setEmployeeName(String employee_name) {
		this.employee_name = employee_name;
	}
}
