package mhat.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	private EditText assetNo;
	private String passNo;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}

	// This method is called at button click because we
	// assigned the name of the to the "On Click property" of the button

	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.barcodeButtonMain:
			Intent intent = new Intent("com.google.zxing.client."
					+ "android.SCAN");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"QR_CODE_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"ONE_D_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"PRODUCT_MODE");
			startActivityForResult(intent, 0);
			break;

		case R.id.searchButton:
			Intent intent1 = new Intent(MainActivity.this,
					mhat.allAssets.AllAssetsActivity.class);
			// Set the asset title of the selected list item
			assetNo = (EditText) findViewById(R.id.assetNoTextBox);
			passNo = assetNo.getText().toString();

			// Pass the variable with the intent to AssetActivity
			intent1.putExtra("strings", passNo);
			startActivity(intent1);
			assetNo.setText("");
			break;

		case R.id.addAssetsButton:
			Intent intent2 = new Intent(MainActivity.this,
					mhat.addAsset.AddAssetActivity.class);
			startActivity(intent2);
			break;

		case R.id.allAssetsButton:
			Intent intent3 = new Intent(MainActivity.this,
					mhat.allAssets.AllAssetsActivity.class);
			startActivity(intent3);
			break;

		case R.id.auditButton:
			Intent intent4 = new Intent(MainActivity.this,
					mhat.audit.AuditActivity.class);
			startActivity(intent4);
			break;

		case R.id.reportsButton:
			Intent intent5 = new Intent(MainActivity.this,
					mhat.reports.ReportsActivity.class);
			startActivity(intent5);
			break;
		}

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				Intent intent1 = new Intent(MainActivity.this,
						mhat.allAssets.AllAssetsActivity.class);

				// Result from Bar-code Scanner
				passNo = intent.getStringExtra("SCAN_RESULT");
				// Pass the variable with the intent to
				// AllAssetsActivity
				intent1.putExtra("strings", passNo);
				startActivity(intent1);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	// Create menu options
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	// This method is called once the menu is selected
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Menu options
		case R.id.syncMenu:

			break;
		}
		return true;
	}
}