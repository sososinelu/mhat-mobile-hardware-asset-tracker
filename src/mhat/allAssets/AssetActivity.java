package mhat.allAssets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import mhat.main.R;
import mhat.sqlite.Asset;
import mhat.sqlite.DatabaseHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AssetActivity extends Activity {
	private String passName;
	String passID;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.asset);

		Intent intent2 = getIntent();
		passID = intent2.getStringExtra("strings");

		TextView assetID = (TextView) findViewById(R.id.assetIDLabel);
		assetID.setText(passID);

		DatabaseHandler db = new DatabaseHandler(this);
		Asset asset = db.getAsset(Integer.parseInt(passID));

		TextView name = (TextView) findViewById(R.id.assetName);
		TextView asset_no = (TextView) findViewById(R.id.assetNo);
		TextView description = (TextView) findViewById(R.id.assetDescription);
		TextView manufacturer = (TextView) findViewById(R.id.assetManufacturer);
		TextView model = (TextView) findViewById(R.id.assetModel);
		TextView serial_no = (TextView) findViewById(R.id.assetSerialNo);
		TextView asset_status = (TextView) findViewById(R.id.assetStatus);
		TextView audit_date = (TextView) findViewById(R.id.assetAuditDate);
		TextView supplier = (TextView) findViewById(R.id.assetSupplier);
		TextView invoice_no = (TextView) findViewById(R.id.assetInvoiceNumber);
		TextView purchase_date = (TextView) findViewById(R.id.assetPurchaseDate);
		TextView warranty_expire = (TextView) findViewById(R.id.assetWarrantyExpDate);
		TextView warranty_no = (TextView) findViewById(R.id.assetWarrantyNo);
		TextView warranty_details = (TextView) findViewById(R.id.assetWarrantyDetails);
		TextView department = (TextView) findViewById(R.id.assetDepartment);
		TextView employee = (TextView) findViewById(R.id.assetEmployee);
		TextView branch = (TextView) findViewById(R.id.assetBranch);
		ImageView thumbImage = (ImageView) findViewById(R.id.assetImage);

		name.setText(asset.getName());
		passName = asset.getName();
		asset_no.setText(asset.getAssetNo());
		description.setText(asset.getDescription());
		manufacturer.setText(asset.getManufacturer());
		model.setText(asset.getModel());
		serial_no.setText(asset.getSerialNo());
		asset_status.setText(asset.getAssetStatus());
		audit_date.setText(asset.getAuditDate());
		supplier.setText(asset.getSupplier());
		invoice_no.setText(asset.getInvoiceNo());
		purchase_date.setText(asset.getPurchaseDate());
		warranty_expire.setText(asset.getWarrantyExpire());
		warranty_no.setText(asset.getWarrantyNo());
		warranty_details.setText(asset.getWarrantyDetails());
		department.setText(asset.getDepartment());
		employee.setText(asset.getEmployee());
		branch.setText(asset.getBranch());

		String imageName = asset.getPictureName();

		// Retrieve the image from the SD card
		File sdCardDirectory = new File(Environment
				.getExternalStorageDirectory().toString() + "/MHAT/Images");
		File image = new File(sdCardDirectory, imageName);

		FileInputStream inStream;
		try {
			inStream = new FileInputStream(image);
			Bitmap bm = BitmapFactory.decodeStream(inStream);
			inStream.close();
			thumbImage.setImageBitmap(bm);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Create menu options
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.asset_menu, menu);
		return true;
	}

	// This method is called once the menu is selected
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Menu options
		case R.id.homeMenu:
			Intent intent = new Intent(AssetActivity.this,
					mhat.main.MainActivity.class);
			startActivity(intent);
			break;

		case R.id.deleteMenu:
			// Create AlertDialog
			final Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.deleteAsset);
			builder.setIcon(R.drawable.delete);
			builder.setCancelable(true);
			final TextView input = new TextView(this);
			input.setTextSize(20);
			input.setText(passName);
			builder.setView(input);
			builder.setPositiveButton("Delete Asset",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							DatabaseHandler db = new DatabaseHandler(
									AssetActivity.this);

							Asset asset1 = db.getAsset(Integer.parseInt(passID
									.toString()));
							db.deleteAsset(asset1);
							Intent intent1 = new Intent(AssetActivity.this,
									mhat.allAssets.AllAssetsActivity.class);
							startActivity(intent1);

							Toast.makeText(getApplicationContext(),
									"Asset Deleted", Toast.LENGTH_LONG).show();
						}
					});
			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Close the alert dialog
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		}
		return true;
	}
}
