package mhat.allAssets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mhat.main.R;
import mhat.sqlite.Asset;
import mhat.sqlite.DatabaseHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AllAssetsActivity extends Activity {
	static String name;
	static String description;
	static String assetNo;
	static String assetID;
	static String assetImage;

	private ArrayList<String> nameList;
	private ArrayList<String> descriptionList;
	private ArrayList<String> assetNoList;
	private ArrayList<String> assetIDList;
	private ArrayList<String> assetImageList;

	private AssetArrayAdapter listAssetArrayAdapter;
	private ListView listView;
	private EditText assetNoSearch;
	private String passID;
	private String passName;
	private String passNo;
	private ArrayList<HashMap<String, String>> assetList;
	private HashMap<String, String> map;
	private List<Asset> asset;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.all);

		/*
		 * Set the data for the List View widget
		 */

		assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAll);
		assetList = new ArrayList<HashMap<String, String>>();

		DatabaseHandler db = new DatabaseHandler(this);
		asset = db.getAllAssets();

		// Array List to store the asset info
		nameList = new ArrayList<String>();
		descriptionList = new ArrayList<String>();
		assetNoList = new ArrayList<String>();
		assetIDList = new ArrayList<String>();
		assetImageList = new ArrayList<String>();

		for (Asset as : asset) {
			name = as.getName();
			nameList.add(name);

			description = as.getDescription();
			descriptionList.add(description);

			assetNo = as.getAssetNo();
			assetNoList.add(assetNo);

			assetID = Integer.toString(as.getID());
			assetIDList.add(assetID);

			assetImage = as.getPictureName();
			assetImageList.add(assetImage);
		}

		for (int i = 0; i < nameList.size(); i++) {
			hashMap(i);
		}

		listView = (ListView) findViewById(R.id.listView);
		// List View adapter object
		listAssetArrayAdapter = new AssetArrayAdapter(this, assetList);
		// Bind List View with adapter
		listView.setAdapter(listAssetArrayAdapter);

		// Click event for single list row item
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent2 = new Intent(AllAssetsActivity.this,
						mhat.allAssets.AssetActivity.class);
				// Set the asset title of the selected list item
				TextView assetID = (TextView) view
						.findViewById(R.id.textViewAssetID);

				passID = assetID.getText().toString();
				// Pass the asset ID with the intent to AssetActivity
				intent2.putExtra("strings", passID);
				startActivity(intent2);
			}

		});

		// Long click event for single list row item
		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					int arg2, long arg3) {
				TextView assetID = (TextView) v
						.findViewById(R.id.textViewAssetID);
				passID = assetID.getText().toString();

				TextView assetName = (TextView) v
						.findViewById(R.id.textViewAssetName);

				passID = assetID.getText().toString();
				passName = assetName.getText().toString();

				return false;
			}

		});

		listView.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {

			@Override
			public void onCreateContextMenu(ContextMenu menu, View v,
					ContextMenuInfo menuInfo) {
				menu.setHeaderTitle(passName);
				menu.add(0, 0, 0, R.string.deleteAsset);
				menu.add(0, 1, 0, R.string.editAsset);
			}
		});

		/*
		 * Set the asset no search box listener
		 */

		assetNoSearch.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				// Abstract method
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// Abstract method
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				assetList.clear();

				String search = assetNoSearch.getText().toString();
				boolean exist = false;

				int searchListLength = assetNoList.size();
				for (int i = 0; i < searchListLength; i++) {
					if (assetNoList.get(i).contains(search)) {
						hashMap(i);
						exist = true;
					}
				}

				if (exist == false) {
					Toast.makeText(getApplicationContext(),
							"Asset " + search + " Not Found" + "",
							Toast.LENGTH_SHORT).show();
					for (int i = 0; i < searchListLength; i++) {
						hashMap(i);
					}
				}
				listView = (ListView) findViewById(R.id.listView);
				listAssetArrayAdapter = new AssetArrayAdapter(
						AllAssetsActivity.this, assetList);
				listView.setAdapter(listAssetArrayAdapter);
			}
		});

		/*
		 * Intent from the Main Activity containing the asset no from search box
		 */

		Intent intent1 = getIntent();
		// Retrieve variable from intent
		passNo = intent1.getStringExtra("strings");
		// Set String variable to search box
		assetNoSearch.setText(passNo);

	}

	// Asset menu selected
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getGroupId() == 0) {
			switch (item.getItemId()) {
			case 0:
				// Create AlertDialog
				final Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.deleteAsset);
				builder.setIcon(R.drawable.delete);
				builder.setCancelable(true);
				final TextView input = new TextView(this);
				input.setTextSize(20);
				input.setText(passName);
				builder.setView(input);
				builder.setPositiveButton("Delete Asset",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								DatabaseHandler db = new DatabaseHandler(
										AllAssetsActivity.this);

								Asset asset1 = db.getAsset(Integer
										.parseInt(passID.toString()));
								db.deleteAsset(asset1);

								assetNoList.clear();
								asset = db.getAllAssets();

								for (Asset as : asset) {
									assetNo = as.getAssetNo();
									assetNoList.add(assetNo);
								}

								// Reset asset list view
								assetList.clear();
								int searchListLength = assetNoList.size();
								for (int i = 0; i < searchListLength; i++) {

									hashMap(i);
								}

								listView = (ListView) findViewById(R.id.listView);
								// List View adapter object
								listAssetArrayAdapter = new AssetArrayAdapter(
										AllAssetsActivity.this, assetList);
								// Bind List View with adapter
								listView.setAdapter(listAssetArrayAdapter);

								Toast.makeText(getApplicationContext(),
										"Asset Deleted", Toast.LENGTH_LONG)
										.show();

							}
						});
				builder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// Close the alert dialog
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
				break;

			case 1:
				Toast.makeText(getApplicationContext(), "Edit",
						Toast.LENGTH_SHORT).show();

				Intent intent3 = new Intent(AllAssetsActivity.this,
						mhat.addAsset.AddAssetActivity.class);
				// Set the asset title of the selected list item
				// Pass the asset ID with the intent
				// to AssetActivity
				intent3.putExtra("strings", passID);
				startActivity(intent3);
				break;
			}
		}
		return false;
	}

	// Listener method to widget (e.g. button) events
	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.barcodeButtonAllAssets:
			Intent intent = new Intent("com.google.zxing.client."
					+ "android.SCAN");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"QR_CODE_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"ONE_D_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"PRODUCT_MODE");
			startActivityForResult(intent, 0);
			break;

		case R.id.clearButtonAll:
			assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAll);
			assetNoSearch.setText("");
			break;
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				// Handle successful scan and display contents
				TextView text_view = (TextView) findViewById(R.id.assetNoTextBoxAll);
				text_view.setText(contents);
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	// Add data to the Hash Map and Array List
	public void hashMap(int i) {
		// Create new HashMap
		map = new HashMap<String, String>();

		// Add data to HashMap
		map.put(name, nameList.get(i));
		map.put(description, descriptionList.get(i));
		map.put(assetNo, assetNoList.get(i));
		map.put(assetID, assetIDList.get(i));
		map.put(assetImage, assetImageList.get(i));

		// Adding HashList to ArrayList
		assetList.add(map);
	}

	// Create menu options
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.asset_menu, menu);
		return true;
	}

	// This method is called once the menu is selected
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Menu options
		case R.id.homeMenu:
			Intent intent = new Intent(AllAssetsActivity.this,
					mhat.main.MainActivity.class);
			startActivity(intent);
			break;

		}
		return true;
	}
	
	public void deleteAlertDialog(){
		
	}

}
