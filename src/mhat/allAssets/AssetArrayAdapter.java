package mhat.allAssets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import mhat.main.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AssetArrayAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater = null;

	public AssetArrayAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null)
			view = inflater.inflate(R.layout.list_row, null);

		TextView nameTextView = (TextView) view
				.findViewById(R.id.textViewAssetName);
		TextView descriptionTextView = (TextView) view
				.findViewById(R.id.textViewDescription);
		TextView assetNoTextView = (TextView) view
				.findViewById(R.id.textViewAssetNo);
		TextView assetIDTextView = (TextView) view
				.findViewById(R.id.textViewAssetID);
		ImageView thumbImage = (ImageView) view
				.findViewById(R.id.imageViewAssetPic);

		HashMap<String, String> listAsset = new HashMap<String, String>();
		listAsset = data.get(position);

		// Setting all the values in the list view
		nameTextView.setText(listAsset.get(AllAssetsActivity.name));
		descriptionTextView.setText(listAsset
				.get(AllAssetsActivity.description));
		assetNoTextView.setText(listAsset.get(AllAssetsActivity.assetNo));
		assetIDTextView.setText(listAsset.get(AllAssetsActivity.assetID));
		String imageName = listAsset.get(AllAssetsActivity.assetImage);

		// Retrieve the image from the SD card
		File sdCardDirectory = new File(Environment
				.getExternalStorageDirectory().toString() + "/MHAT/Images");
		File image = new File(sdCardDirectory, imageName);

		FileInputStream inStream;

		try {
			inStream = new FileInputStream(image);
			Bitmap bm = BitmapFactory.decodeStream(inStream);
			inStream.close();
			thumbImage.setImageBitmap(bm);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return view;
	}
}
