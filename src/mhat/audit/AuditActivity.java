package mhat.audit;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mhat.main.R;
import mhat.sqlite.Asset;
import mhat.sqlite.DatabaseHandler;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AuditActivity extends Activity {
	private EditText assetNoSearch;
	private TextView assetName;
	private TextView auditDate;
	private Button assetCalendar;
	private Button save;
	private Button cancel;

	private String assetNo;
	private String assetID;
	private String search;
	private int id;
	private boolean exist = false;
	private ArrayList<String> assetNoList;
	private ArrayList<String> assetIDList;
	private DatabaseHandler db = new DatabaseHandler(this);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audit);

		assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAudit);

		List<Asset> asset = db.getAllAssets();

		// Array List to store the asset info
		assetNoList = new ArrayList<String>();
		assetIDList = new ArrayList<String>();

		for (Asset as : asset) {
			assetNo = as.getAssetNo();
			assetNoList.add(assetNo);

			assetID = Integer.toString(as.getID());
			assetIDList.add(assetID);
		}

		/*
		 * Set the asset no search box listener
		 */

		assetNoSearch.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				// Abstract method
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// Abstract method
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				search = assetNoSearch.getText().toString();
				// search = assetNoSearch.getText().toString();

				int searchListLength = assetNoList.size();
				for (int i = 0; i < searchListLength; i++) {
					if (assetNoList.get(i).contains(search)) {
						exist = true;
						id = Integer.parseInt(assetIDList.get(i));
						Asset asset = db.getAsset(Integer.parseInt(assetIDList
								.get(i)));

						assetName = (TextView) findViewById(R.id.assetName);
						auditDate = (TextView) findViewById(R.id.labelAuditDateSet);
						assetCalendar = (Button) findViewById(R.id.assetCalendarButton);
						save = (Button) findViewById(R.id.saveButton);
						cancel = (Button) findViewById(R.id.cancelButton);
						// Asset name is visible
						assetName.setVisibility(View.VISIBLE);
						// Enable buttons
						assetCalendar.setEnabled(exist);
						save.setEnabled(exist);
						cancel.setEnabled(exist);
						// Get values from database and set them
						assetName.setText(asset.getName());
						auditDate.setText(asset.getAuditDate());
					}
				}
				if (exist == false) {
					Toast.makeText(getApplicationContext(),
							"Asset " + search + " Not Found" + "",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void myClickHandler(View view) {
		switch (view.getId()) {
		case R.id.barcodeButtonAudit:
			Intent intent = new Intent("com.google.zxing.client."
					+ "android.SCAN");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"QR_CODE_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"ONE_D_MODE");
			intent.putExtra("com.google.zxing.client.android.SCAN.SCAN_MODE",
					"PRODUCT_MODE");
			startActivityForResult(intent, 0);
			break;

		case R.id.clearButtonAudit:
			assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAudit);
			assetNoSearch.setText("");
			exist = false;
			assetName = (TextView) findViewById(R.id.assetName);
			auditDate = (TextView) findViewById(R.id.labelAuditDateSet);
			assetCalendar = (Button) findViewById(R.id.assetCalendarButton);
			save = (Button) findViewById(R.id.saveButton);
			cancel = (Button) findViewById(R.id.cancelButton);

			// Asset name is visible
			assetName.setVisibility(View.INVISIBLE);
			// Disable buttons
			assetCalendar.setEnabled(exist);
			save.setEnabled(exist);
			// cancel.setEnabled(exist);
			auditDate.setText("--/--/--");

			break;

		case R.id.assetCalendarButton:
			Calendar cal = Calendar.getInstance();
			DatePickerDialog datePickDiag = new DatePickerDialog(
					AuditActivity.this, odsl, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			datePickDiag.show();
			break;

		case R.id.saveButton:
			auditDate = (TextView) findViewById(R.id.labelAuditDateSet);
			assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAudit);
			search = assetNoSearch.getText().toString();

			int searchListLength = assetNoList.size();
			for (int i = 0; i < searchListLength; i++) {
				if (assetNoList.get(i).contains(search)) {
					id = Integer.parseInt(assetIDList.get(i));

					Asset asset = db.getAsset(Integer.parseInt(assetIDList
							.get(i)));
					asset.setAuditDate(auditDate.getText().toString());
					db.updateAsset(asset);
				}
			}
			Toast.makeText(getApplicationContext(), "Audit Completed",
					Toast.LENGTH_SHORT).show();
			finish();
			break;

		case R.id.cancelButton:
			finish();
			break;

		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				// Handle successful scan and display contents
				assetNoSearch = (EditText) findViewById(R.id.assetNoTextBoxAudit);
				assetNoSearch.setText(intent.getStringExtra("SCAN_RESULT"));
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	final OnDateSetListener odsl = new OnDateSetListener() {
		public void onDateSet(DatePicker arg0, int year, int month,
				int dayOfMonth) {
			month++;
			auditDate = (TextView) findViewById(R.id.labelAuditDateSet);
			auditDate.setText(dayOfMonth + "/" + month + "/" + year);
		}
	};

	// Create options menu
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_menu, menu);
		return true;
	}

	// This method is called once the menu is selected
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Menu options
		case R.id.homeMenu:
			Intent intent = new Intent(AuditActivity.this,
					mhat.main.MainActivity.class);
			startActivity(intent);
			break;
		}
		return true;
	}
}
